[OpenCor Forum](https://opencor.gitlab.io/)


**What is it?**

Firstly, OpenCor is a **collaborative** list of open corpora for Latin American 
and Iberian Languages. OpenCor is also a discussion group aiming to have annual 
meetings to integrate these communities.

Everyone interested in the topic is welcome to join the 
[group](https://groups.google.com/forum/#!forum/opencor)

**Goals**

* Easy access/discovery of Latin American and Iberian Languages resources
* Integration
* Discussion
* Share ideas and resources
* Publish work on corpora
* Visibility
* Support

This year OpenCor Forum was held in Canela, Brazil as part of [PROPOR 
2018](http://www.inf.ufrgs.br/propor-2018):

* [Call for papers](https://opencor.gitlab.io/opencor-2018/)
* [List of acepted papers](https://opencor.gitlab.io/accepted-opencor-2018/)
* [Program](https://opencor.gitlab.io/program/)
