---
id: rigo21eticagem
title: "Etiquetagem morfossintática automática de corpora de língua falada transcrita"
abstract:
    en: ""
authors_abstract:
    - Mônica Rigo
year: 2021
category: software
type_: methodology
languages:
    - Brazilian Portuguese
languages_codes:
    - pt-BR
glottolog:
    - http://glottolog.org/resource/languoid/id/braz1246
date: 2021-12-02T05:00:00-00:00
showDate: true
draft: false
type: corpora
status: In development
---

We describe in this work the corpus FRACAS-BR, a preliminary translation of the
FraCaS corpus, which had the original goal of benchmarking a large selection of
English semantic phenomena as far as inference is concerned. We hope to
investigate whether these semantic phenomena have corresponding ones in
Portuguese and whether native speakers agree with the official inference
annotations.
