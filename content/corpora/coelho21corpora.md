---
id: coelho21corpora
title: "Corpora do Calendário de Saúde"
abstract:
    en: ""
corpus:
    name: "Corpora do Calendário de Saúde"
    link: "https://github.com/leoGCoelho/Corpora-Calendario-da-Saude"
authors_abstract:
    - Leonardo Coelho
    - Larissa Freitas
year: 2021
category: resource
type_: text
languages:
    - Brazilian Portuguese
languages_codes:
    - pt-BR
glottolog:
    - http://glottolog.org/resource/languoid/id/braz1246
date: 2021-12-02T05:00:00-00:00
showDate: true
draft: false
type: corpora
---

The "Corpora do Calendário de Saúde" is composed of tweets about campaigns in
the Brazilian health calendar. The texts were extracted from Twitter, composing
14 corpora (one for each campaign) and more than 2 million tweets extracted in
total.
