---
id: sanchez19construcción
title: "Construcción del Corpus Periodístico del Noroeste de México (COPENOR)"
abstract:
    en: ""
corpus:
    name: "COPENOR"
    link: "https://gitlab.com/manuel.wortens/copenor"
contact:
    name: Manuel Alejandro Sánchez-Fernández
    email: manuel.sanchez@colmex.mx
status: "in development"
authors_abstract:
    - Manuel Sánchez-Fernández 
    - Alfonso Medina-Urrea
links: !!omap
    - pre-print: ""
year: 2019
type: text
category: resource
languages:
    - Mexican Spanish
languages_codes:
    - es-MX
glottolog:
    - https://glottolog.org/resource/languoid/id/amer1254
date: 2019-10-08T07:00:00-00:00
showDate: true
draft: false
type: corpora
---

En el marco de la creación de la tesis doctoral que lleva por nombre tentativo “Eti-
quetador automático de los estados de activación en el español del noroeste de Méxi-
co”, se plantea la construcción de COPENOR. Este corpus recolecta 380 notas de 92
medios de comunicación del noroeste de México, región que abarca los estados de
Baja California, Baja California Sur, Sonora, Chihuahua, Durango y Sinaloa. El obje-
tivo de este corpus es servir como base para el análisis de un grupo de categorías
pragmáticas que pertenecen a la Estructura de la Información, llamadas “Estados de
Activación” [1]. La meta de este análisis es desarrollar estrategias para identificar y
etiquetar estas categorías de manera automática.

La creación de este corpus la hemos dividido en dos etapas. En la primera etapa se
recolectaron los medios de cada estado a partir de la base de datos de dos páginas de
internet [2, 3]. Posterior a esto, se corroboró con dos periodistas de Baja California
que no hiciera falta algún medio importante. Se consultó cada medio para revisar que
tuvieran una producción de por lo menos una nota local a la semana. La muestra de la
cantidad de notas se tomó a partir de considerar que un medio productivo genera
aproximadamente 10 notas al día. Con esto, llegamos a que los medios del noroeste
estarían generando 57,000 notas en una ventana temporal de dos meses. Una muestra
de este universo tomando un 95% de confianza y un 5% de margen de error dio como
resultado 380 notas. Con un programa en Python, se asignó un muestreo aleatorio por
conglomerado (tomando como parámetro cada estado) y se recolectaron diariamente 7
notas entre el 23 de mayo y el 16 de julio del 2019, notas que fueron guardadas en un
documento XML. Un segundo paso, que inicia en agosto de este año, es el etiquetado
gramatical utilizando EAGLES, el análisis y etiquetado de Frases Nominales, y el
análisis y asignación de la propiedad del Estado de Activación. En la presentación se
pretende mostrar el proceso a partir del cual se etiquetan a mano estas propiedades
gramaticales y el uso del Diccionario del Español de México para asistir a esta tarea.
La pertinencia de este corpus reside principalmente en las variaciones dialectales que
existen en el noroeste de México, considerado desde hace años como zona dialectal
[4, 5], además de la nula existencia de un corpus del español mexicano etiquetado con
estas propiedades discursivas.


Referencias

1. Lambrecht, K., Information structure and sentence form: topic, focus, and the mental rep-
   resentations of discourse referents. Cambridge University Press, New York (1994).
2. ABYZ News Links http://www.abyznewslinks.com/
3. Prensa Escrita http://www.prensaescrita.com/
4. Serrano, J. C., ¿Existe el noroeste mexicano como zona dialectal? Un acercamiento per-
   ceptual. En: M endoza Guerrero, E., López Berríos, M . y M oreno Rojas, I. E. (eds.)
   LENGUA, LITERATURA Y REGIÓN, pp. 107-130. Universidad Autónoma de Sinaloa,
   M éxico (2009).
5. M endoza Guerrero, E., El Español del noroeste mexicano: un acercamiento desde adentro.
   En Cestero M ancera, A. M ., M olina M artos, I. y Paredes García, F. (eds.) ESTUDIOS
   SOCIOLINGÜÍSTICOS DEL ESPAÑOL DE ESPAÑA Y AM ÉRICA, pp. 159-167. Arco
   Libros, M adrid (2006).
