---
id: perezespinoza19iescchild
title: "IESC-Child: An Interactive Emotional Children’s Speech Corpus"
abstract:
    en: ""
corpus:
    name: "IESC-Child"
    link: ""
links:
    - journal: "https://doi.org/10.1016/j.csl.2019.06.006"
authors_abstract:
    - Humberto Pérez-Espinosa
    - Juan Martínez-Miranda
    - Ismael Espinosa-Curiel
    - Josefina Rodríguez-Jacobo
    - Luis Villaseñor-Pineda
    - Himer Avila-George
links: !!omap
    - pre-print: ""
year: 2019
type: speech
category: resource
languages:
    - Mexican Spanish
languages_codes:
    - es-MX
glottolog:
    - https://glottolog.org/resource/languoid/id/amer1254
date: 2019-10-08T04:00:00-00:00
showDate: true
draft: false
type: corpora
---

Computational Paralinguistics involves analyzing the expressions of users to
obtain information about individual characteristics and different mental states.
To date, most research on paralinguistic information has targeted primarily adult
speech and, to a lesser extent, child speech, even though children are potential
beneficiaries of computers with speech-based interfaces (e.g., for educational applications and entertainment purposes). Current children’s speech corpora have
been designed primarily for tasks not related to paralinguistic analysis, such
as speech recognition or the study of acoustic properties. Few of these corpora
contain genuine interactions that comprise annotations of paralinguistic phenomena. Therefore, to create new emotional models that help develop more advanced
speech-based interactive systems or models designed for children, new corpora
of child-computer interactions that complement the existing corpora are needed.

To contribute to the analysis of paralinguistic information in children’s speech
we created the IESC-Child. This corpus consists of 2,093 minutes of audio recordings (34.88 hours) divided into 19,283 speech segments of the interactions of 174
children (both sexes) between the ages of 6 and 11 years who are speaking in
Mexican Spanish. The recordings were manually segmented and transcribed by
seven human annotators. The native language of each annotator was Spanish.
IESC-Child contains two types of paralinguistic information labels: emotions
(anger, disgust, fear, happiness, sadness, and surprise) and attitudes (confident,
uncertain, apathetic and enthusiastic). For the sake of simplicity, we obtained
the final classes for the two labeling categories by plurality vote across all the
annotators.

To produce different affective reactions in the participants, we used a Wizard
of Oz (WoZ) scenario where children participated in interactive sessions with two
Lego Mindstorms behaving either collaboratively or noncollaboratively. This corpus can facilitate the study of affective reactions in speech communication during
child-computer interaction in Spanish and the creation of models for the recognition of acoustic paralinguistic information. The Interactive Emotional Children’s
Speech Corpus (IESC-Child) can be a valuable resource for researchers given that
offers some advantages over currently available databases such as its size, naturalness, and the type of information annotated. IESC-Child is publicly available
for further analysis of the collected data and further research.
