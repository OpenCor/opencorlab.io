---
id: rezende21anonymization
title: "Anonymization of the B2W-Reviews01 corpus"
abstract:
    en: ""
corpus:
    name: "B2W-Reviews01"
    link: "https://github.com/b2wdigital/b2w-reviews01"
authors_abstract:
    - Fernando Rezende
    - Lucas dos Santos
    - Livy Real
links: !!omap
    - pre-print: ""
prev_version:
    name: "B2W-Reviews01"
    link: "https://opencor.gitlab.io/corpora/real19b2wreviews01/"
year: 2021
type_: text
category: resource
languages:
    - Brazilian Portuguese
languages_codes:
    - pt-BR
glottolog:
    - https://glottolog.org/resource/languoid/id/braz1246
date: 2021-12-02T05:00:00-00:00
showDate: true
draft: false
type: corpora
---

This paper describes the anonymization of data from the
B2W-Reviews01 corpus, removing customer confidential information present
in the texts.
