---
id: rascon19aira
title: "AIRA: Acoustic Interactions for Robot Audition"
abstract:
    en: ""
corpus:
    name: "AIRA"
    link: "https://aira.iimas.unam.mx"
authors_abstract:
    - Caleb Rascón
    - Ivette Velez
links: !!omap
    - pre-print: ""
year: 2019
type: speech
category: resource
languages:
    - Mexican Spanish
languages_codes:
    - es-MX
glottolog:
    - https://glottolog.org/resource/languoid/id/amer1254
date: 2019-10-08T06:00:00-00:00
showDate: true
draft: false
type: corpora
---


El corpus Acoustic Interactions for Robot Audition (AIRA) se compone de
una serie de grabaciones multi-canal que se pueden utilizar para la evaluación
y entrenamiento de algoritmos de localización y separación de fuentes sonoras.
Dichas grabaciones se llevaron a cabo usando dos arreglos de micrófonos dife-
rentes: uno triangular y otro posicionado sobre una estructura 3D que cuenta
con 16 micrófonos. Los micrófonos son omnidireccionales con una respuesta pla-
na en el rango de 50 a 17000 Hz, sobrepasando el rango frecuencial de voz. Se
grabaron hasta 4 fuentes simultáneas, ambas estáticas y móviles. En el caso de
las fuentes estáticas, se utilizaron bocinas con respuesta plana a frecuencia, re-
produciendo grabaciones del corpus DIMEx100 el cual posee a 100 locutores que
leen enunciados balanceados en términos lingüı́sticos, en idioma español y con
muy poco ruido y reverberación. En el caso de fuentes móviles, se realizaron
grabaciones con humanos que caminaban alrededor del arreglo de micrófonos
leyendo enunciados del corpus DIMEx100.

Dichas grabaciones fueron llevadas a cabo en 6 escenarios reales diferentes:

 1. Una cámara anecoica, la cual tiene casi nula reverberación y presencia
    de ruido, y se encuentra en el Laboratorio de Acústica y Vibraciones del
    Instituto de Ciencias Aplicadas y Tecnologı́a (ICAT; antes conocido como
    el Centro de Ciencias Aplicadas y Desarrollo Tecnológico, CCADET) de la
    Universidad Nacional Autónoma de México (UNAM). Se grabaron solamente
    fuentes sonoras estáticas, con ambos arreglos.
 2. La cafeterı́a de la Faculta de Quı́mica de la UNAM, con un nivel conside-
    rable de reverberación y presencia de ruido. Se grabaron solamente fuentes
    sonoras estáticas, con ambos arreglos.
 3. Un supermercado tipo “grande almacén” llamado Tienda UNAM, donde
    hay una presencia considerable de reverberación y ruido (aunque menor a la
    de la cafeterı́a). Se grabaron solamente fuentes sonoras estáticas, con ambos
    arreglos.
 4. El pasillo del Departamento de Ciencias de la Computación (DCC) del Ins-
    tituto de Investigaciones en Matemáticas Aplicadas y en Sistemas (IIMAS),
    el cual cuenta con una reverberación moderada y poco ruido. Se grabaron
    fuentes móviles con el arreglo triangular.
 5. Dos laboratorios de estudiantes del DCC del IIMAS, los cuales cuentan con
    una reverberación moderada y una presencia también moderada de ruido.
    Se grabaron ambas fuentes estáticas y móviles con ambos arreglos.
 6. También se incluyen grabaciones hechas con drones aéreos que, aunque no
    incluyen grabaciones de lenguaje, son de interés para la comunidad de Au-
    dición Robótica, ası́ como de Análisis de Escenas Auditivas.

Para cada sesión de grabación de fuentes estáticas, se provee la siguiente
información: 1) la grabación multi-canal, 2) las grabaciones limpias de cada
fuente, 3) la transcripción de cada fuente (extraı́da del corpus DIMEx100, la
cual a su vez fue capturada manualmente), 4) dirección de arribo de cada fuente.
Para las sesiones con fuentes móviles, no se provee las grabaciones limpias, pero
se provee la trayectoria de cada fuente estimada por medio de un sistema de
seguimiento láser.

Este corpus ha sido utilizado para la evaluación de algoritmos de localización
de fuentes móviles, y está actualmente siendo utilizado para la evaluación de
sistemas de identificación y separación de locutores basados en redes neurona-
les profundas. Esto es relevante, ya que hay varias aplicaciones de interaccción
humano-robot que pueden ser beneficiadas, donde el robot puede jugar el rol de:

a. un mesero en un restaurante ruidoso, b) un ayudante en un supermercado o
mercado de productores (farmer’s market), c) el camarero de un bar (barman),

b. un presentador de un concurso de cultura general, etc.

El corpus completo, ası́ como una documentación detallada de éste, se puede
encontrar en el sitio: https://aira.iimas.unam.mx
