---
id: nunes21morphbr
title: "MorphoBr: Um Dicionário Aberto de Alta Cobertura de Formas Plenas para Análises Morfológicas do Português"
abstract:
    en: ""
corpus:
    name: "MorphoBr"
    link: "https://github.com/LR-POR/MorphoBr"
authors_abstract:
    - Ana Nunes
    - Wellington Silva
    - Leonel Figueiredo
    - Alexandre Rademaker
year: 2021
category: resource
type_: dictionary
languages:
    - Brazilian Portuguese
languages_codes:
    - pt-BR
glottolog:
    - http://glottolog.org/resource/languoid/id/braz1246
date: 2021-12-02T05:00:00-00:00
showDate: true
draft: false
type: corpora
---

MorphoBr [1] é um dicionário eletrônico de alta cobertura do português. Suas
entradas foram construídas inicialmente a partir da consolidação e correção de
outros recursos lexicais, a saber, Label-Lex [2] e DELAF-PB [3]. Posteriormente,
incorporou centenas de milhares de novas entradas geradas automaticamente por
meio de regras de formação de palavras, tal como descrito em [1,5].

Atualmente, desconsiderando verbos com pronomes clíticos, o MorphoBr contabiliza
3475719 entradas, sendo 543694 adjetivos, 21182 advérbios, 2372762 verbos e
538081 substantivos. Cada entrada é constituída por uma forma flexionada
acompanhada de seu lema e tags com informações morfológicas.

Nos arquivos de adjetivos e substantivos existem entradas unificadas, criadas
para evitar a duplicação de entradas que possuem mesma forma e lema, contudo
podem ser classificadas de maneiras diferentes em gênero ou número. Por exemplo,
a forma simples, que pode ser classificada como plural ou singular, masculino ou
feminino, possui apenas uma entrada, que omite essas interpretações para
sinalizar que qualquer uma delas é válida, em vez de possuir quatro entradas.

Como a maioria dos recursos linguísticos de grande escala produzidos
automaticamente, no MorphoBr ainda existem entradas espúrias. Contudo, devido à
sua constante aplicação na revisão do treebank Universal Dependencies (UD)
Portuguese Bosque [6] e na expansão da cobertura lexical da PorGram [4], uma
nova gramática no formalismo da HPSG, centenas de milhares de falhas do recurso
têm sido detectadas e corrigidas constantemente.


**Reference**

1. de Alencar, L.F., Cuconato, B., Rademaker, A.: MorphoBr: An open source large-
coverage full-form lexicon for morphological analysis of Portuguese. Texto
Livre: Linguagem e Tecnologia 11(3), 1–25 (2018), http://www.periodicos.letras.
ufmg.br/index.php/textolivre/article/view/14294

2. Eleutério, S., Freire, H., Ranchhod, E., Baptista, J.: A system of electronic
dictionaries of Portuguese. Lingvisticae Investigationes 19(1), 57–82 (1995).
https://doi.org/https://doi.org/10.1075/li.19.1.04ele

3. Muniz, M.C.M.: A construção de recursos linguı́stico-computacionais
para o português do brasil: o projeto de unitex-pb. Master’s the-
sis, Instituto de Ciências Matemáticas e de Computação, USP (2004).
https://doi.org/10.11606/D.55.2020.tde-19022020-151305

4. Nunes, A.L., Rademaker, A., de Alencar, L.F.: Utilizando um dicionário
morfológico para expandir a cobertura lexical de uma gramática do português no formalismo
hpsg. In: Proceedings of the 13th Brazilian Symposium in Information and Human
Language Technology. Brazil, November 2021. p. to appear (2021)

5. Silva, H.L.B.: Expansão do MorphoBr através da modelagem computacional de pro-
cessos de formação de palavras em português. Master’s thesis, Universidade
Federal

6. do Ceará, Brazil (2019), http://www.repositorio.ufc.br/handle/riufc/47136
Silva, W., Rademaker, A., de Alencar, L.F.: Explorando a revisão de corpora por
meio da comparação de regras gramaticais em padrões sintáticos. In: Proceedings
of the 13th Brazilian Symposium in Information and Human Language Technology.
Brazil, November 2021. p. to appear (2021)

