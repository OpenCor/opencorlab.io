---
id: ramom20carvalho
title: "Carvalho, um corpus diacrónico em ortografia original para português, espanhol e inglês"
abstract:
    en: ""
corpus:
    name: "Carvalho"
    link: ""
authors_abstract:
    - José Ramom Pichel
    - Pablo Gamallo Otero
    - Marco Neves
    - Iñaki Alegria.
links: !!omap
    - pre-print: ""
year: 2020
type: text
category: resource
languages:
    - Portuguese
    - Spanish
    - Enlgihs
languages_codes:
    - pt
    - es
    - en
glottolog:
    - 
    - https://glottolog.org/resource/languoid/id/stan1288
    - https://glottolog.org/resource/languoid/id/stan1293
date: 2020-03-01T07:00:00-00:00
showDate: true
draft: false
type: corpora
---

#### Introdução

Existem diferentes corpora de português, espanhol e inglês livres, como o Ty-
cho Brahe corpus<sup>1</sup> [2], Colonia<sup>2</sup> [5], Project Gutenberg<sup>3</sup> , OpenLibrary<sup>4</sup> , Domı́nio
Público<sup>5</sup> ou o Wiki source<sup>6</sup> , que contêm textos ora na ortografia original, ora
editados e até adaptados à ortografia atual de cada lı́ngua.

No entanto, um corpus histórico que contenha apenas textos em ortografia
original pode ajudar a investigar o papel que a ortografia desempenha na identificação automática das lı́nguas, em investigações de linguı́stica histórica ou na
medição automática de distância entre lı́nguas. Por esta razão, foi desenhado o
corpus diacrónico Carvalho que só contém documentos na ortografia original. O
corpus está disponı́vel no site7 .

Carvalho é um corpus diacrónico para três lı́nguas: Carvalho-PT-PT (Português Europeu), Carvalho-ES-ES (Espanhol Europeu), Carvalho-EN-UK (Inglês
do Reino Unido); e para a variante brasileira do português (Carvalho-PT-BR) e
a variante argentina do espanhol (Carvalho-ES-AR).

Atendendo à classificação fornecida pelo Corpus Helsinki [4], inicialmente
o corpus diacrónico Carvalho seria dividido em: perı́odo medieval (XII-XV),
era moderna (XVI-XVIII) e era contemporânea (XIX-XX). No entanto, como o
português e espanhol têm um grande volume de textos e padrões ortográficos
diferentes nos séculos XIX e XX, decidimos dividir estes dois séculos em dois
sub-perı́odos (XIX-1, XIX-2, XX-1 e XX-2) para todas as lı́nguas (português,
espanhol e inglês). No que diz respeito às variantes diatópicas do português
do Brasil e do espanhol da Argentina, criamos corpora para os sub-perı́odos
recentes: segunda metade do século XX (XX-2) e o século XXI até o presente
(XXI-1).

No que toca à dimensão do corpus, seguimos os critérios de dois autores do
Helsinki Corpus of Historical English [3], que indicam que: “O tamanho do cor-
pus básico é de perto de 1,5 milhões de palavras”. Portanto, todos os perı́odos
históricos (XII-XV, XVI-XVIII, XIX-1, XIX-2, XX-1, XX-2) em todos os idiomas (português, espanhol, inglês) e variantes (português do Brasil, espanhol
da Argentina) nos perı́odos XX-2 e XXI-1, têm no mı́nimo essa dimensão.

Finalmente, para tornar o corpus Carvalho representativo, tendo em conta a
representatividade definida por [1], incluı́mos sistematicamente para cada perı́odo
50% textos de ficção e 50% de não-ficção.



<sup>1</sup>
    http://www.tycho.iel.unicamp.br/corpus/index.html

<sup>2</sup>
    http://corporavm.uni-koeln.de/colonia/

<sup>3</sup>
    https://www.gutenberg.org/browse/languages/es

<sup>4</sup>
    https://openlibrary.org/

<sup>5</sup>
    http://www.dominiopublico.gov.br

<sup>6</sup>
    https://en.wikisource.org

<sup>7</sup>
    https://XXXXXXXXXXXXXXXXXXXXXXXXX

References

1. Biber, D.: Representativeness in corpus design. Literary and linguistic computing
   8(4), 243–257 (1993)
2. Galves, C., Faria, P.: Tycho Brahe parsed corpus of historical Portuguese. URL:
   http://www. tycho. iel. unicamp. br/˜ tycho/corpus/en/index. html (2010)
3. Rissanen, M., Kytö, M., Palander-Collin, M.: Early English in the computer age:
   Explorations through the Helsinki Corpus. No. 11, Walter de Gruyter (1993)
4. Rissanen, M., et al.: The helsinki corpus of english texts. Kyttö et. al pp. 73–81
   (1993)
5. Zampieri, M.: Compiling and processing historical and contemporary portuguese
   corpora. arXiv preprint arXiv:1710.00803 (2017)

