---
title: Previous editions
date: 2018-09-17
---

OpenCor 2020  was part of [PROPOR 2020](https://propor.di.uevora.pt/): 

* [Call for papers]({{< relref "cfp2020">}}) 
* [Program]({{< relref "program2020">}}) 
* [List of accepted talks]({{< relref "accepted2020">}})


OpenCor 2019 was part of the [PLAGAA 2019](https://sites.google.com/view/plagaa2019) metting.

* [Call for papers]({{< relref "cfp2019">}})
* [List of accepted talks]({{< relref "accepted2019">}})
* [Program]({{< relref "program2019">}})


OpenCor 2018 was part of [Propor 2018](http://www.inf.ufrgs.br/propor-2018)

* [Call for papers]({{< relref "cfp2018">}})
* [List of accepted talks]({{< relref "accepted2018" >}})
* [Program]({{< relref "program2018" >}})
