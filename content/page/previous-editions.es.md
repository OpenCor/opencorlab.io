---
title: Ediciones previas
date: 2018-09-17
---

OpenCor 2020 fue parte de [PROPOR 2020](https://propor.di.uevora.pt/): 

* [Call for papers]({{< relref "cfp2020">}}) 
* [Program]({{< relref "program2020">}}) 
* [List of accepted talks]({{< relref "accepted2020">}})


OpenCor 2019 fue parte de la reunión [PLAGAA 2019](https://sites.google.com/view/plagaa2019).

* [Call for papers]({{< relref "cfp2019">}})
* [List of accepted talks]({{< relref "accepted2019">}})
* [Program]({{< relref "program2019">}})


OpenCor 2018 fue parte de [PROPOR 2018](http://www.inf.ufrgs.br/propor-2018)

* [Call for papers (Inglés)](/cfp2018/)
* [List de artículos aceptados (Inglés)](/accepted-opencor-2018/)
* [Programa (Inglés)](/program-2018/)
