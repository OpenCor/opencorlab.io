---
title: "CFP: OpenCor 2021"
date: 2019-02-01
---

This would be the fourth edition of OpenCor, an annual venue that aims to gather
the community working on freely available language resources for the large
variety of languages spoken in Iberian countries and in Latin America.

Recent years have seen a move in Computational Linguistics towards bigger and
better, more reliably annotated corpora. However, the existence of such reliably
annotated corpora is one of the big bottlenecks for processing natural language.
Producing and maintaining corpora is a hard task that most of the time requires
sizeable funding and the cooperation of several experts. Although having such
corpora available is clearly essential, the many difficulties and the amount of
work needed to produce reliable corpora make the process of producing this data
and making it available a non-trivial proposition. While “big data” is a trend,
producing reliable corpora continues to be an invisible task in Natural Language
Processing. Especially when working on languages different from English, on
smaller datasets not immediately suitable for machine learning approaches, or on
a new release of a previous dataset, it is not obvious to the corpora creators
how to publish and properly discuss their work. Most of the biggest Natural
Language Processing venues are not open to accepting corpora descriptions. The
situation is even worse when considering minority languages and endangered
languages since most of them do not have a related venue where these works can
be discussed.

The Latin American and Iberian communities that produce open corpora do not have
an established event that would make it possible for experts to share ideas,
discuss difficulties, and get feedback on their work. Different meetings have
been held in the last years, but either they are not generic enough to embrace
all corpora work done in these communities, or there was no continuation and
support for future editions. Due to these conditions, it is no rare that groups
that share related interests or face the same difficulties are not aware of
other groups and their recent work within these communities.

This forum aims both to fill the gap of having a permanent venue for
construction, annotation, and maintenance of open corpora for Latin American and
Iberian languages and to create an extensive list of these resources. OpenCor
welcomes discussions on Portuguese, Spanish, indigenous languages, creoles,
Galician, Catalan, Aragonese, Astur-Leonese, Aranese, and any other language
spoken in Latin America and Iberian countries. Work on endangered languages,
minority, and/or less-resourced languages is particularly welcome.

## The venue

This is the fourth edition of OpenCor Forum, an attempt to gather the community that produces, maintains and makes freely available language resources for the large variety of languages spoken in Iberian countries and in Latin America. All accepted works will also be part of the OpenCor list, an initiate to have cataloged open resources produced for the targeted languages.
This forum welcomes, but it is not restricted to, the following topics:

* releases of new open data sets
* descriptions of established open corpora
* guidelines creation, annotation strategies, and best practices discussion
* corpora maintenance and management
* corpora curation and assessment
* corpora design and evaluation
* corpora creation strategies and difficulties faced by the community
* ethical aspects of corpora creation

This year OpenCor Forum will be held online, as a part of [STIL 2021 - Symposium
in Information and Human Language Technology](http://comissoes.sbc.org.br/ce-pln/stil2021/index.html):


## Submission

We invite submissions of anonymized extended abstracts up to one page, with references. The documents should be anounimous. Documents must follow Springer LNCS and must be submitted in Iberian, Latin American Language or English. Accepted extended abstracts will serve as the submitted corpus description on the OpenCor List. OpenCor is non-archival, therefore works that have been or are planned to be published elsewhere are also welcome.

Authors need to submit together with the extended abstract the link for their resources. One of the goals of OpenCor is to provide a full list of resources and described languages by the end of the forum. We hope this list will be helpful in keeping track of freely available resources for our targeted languages.

Considering that one of the main challenges for these communities is funding raising, all accepted works will be available in the forum page and will appear in the resources list, even if no author can attend the forum. If the authors attend there will be the chance to give an extended talk during the forum, if not we will ask for a five-minute video on any video-online platform that will be projected during the event. Authors must indicate in the moment of submission how they want to participate in the OpenCor Forum 2020. This is an attempt to create an extensive list of open corpora available that does not rely on how much funding the working groups have. 
 
- [Submission link](https://easychair.org/conferences/?conf=opencor2021forum) </br>
- [LaTex stylesheet](https://resource-cms.springernature.com/springer-cms/rest/v1/content/19238648/data/v1) </br>
- [MS Word stylesheet](https://resource-cms.springernature.com/springer-cms/rest/v1/content/19238706/data/v1)</br>
- [LaTex in overleaf](https://www.overleaf.com/latex/templates/springer-lecture-notes-in-computer-science/kzwwpvhwnvfj#.WuA4JS5uZpi) 

## Schedule

* September 08th: First Call for Papers 
* September 30th: Second Call for papers
* Deadline for submissions: <del>October 13th</del> October 21th
* Acceptance: November 1st
* Session: to be agreed with STIL organizers (November 29th to December 3rd, 2021)


### Forum Registration

* To be announced


### Organization

* Livy Real – americanas s.a. Digital Lab - livyreal [at] gmail.com

* Ivan Vladimir Meza Ruiz –  IIMAS/UNAM


### Program Committee

* Samuel González-López, Technological University of Nogales
* Amália Mendes, Centro de Linguística da Universidade de Lisboa
* Ximena Gutierrez, University of Zürich
* Gabriela Ramírez-De-La-Rosa, Universidad Autónoma Metropolitana Unidad Cuajimalpa
* Renata Vieira, PUCRS
* Carlos Daniel Hernández Mena, Reykjavík University
* Marcos Garcia, Universidade de Santiago de Compostela
* Manuel Sánchez, El Colegio de Méxio
* Humberto Pérez-Espinosa, CICESE-UT3
* Fernanda López, Universidad Nacional Autónoma de México
* Víctor Mijangos de La Cruz, Universidad Nacional Autónoma de México
* Ignacio Arroyo, Universidad Tecnológica de la Mixteca
* Manuel Mager, University of Stuttgart
* Aline Villavicencio, University of Sheffield  and Federal University of
    Rio Grande do Sul 


## Contact

Any question may be sent to the organizers: livyreal [at] gmail.com; ivanvladimir [at] turing.iimas.unam.mx



