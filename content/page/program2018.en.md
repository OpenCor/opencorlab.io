
---
title: Program 2018
description: Canela, Brazil, as a part of PROPOR 2018
date: 2018-09-17
---

<div class='container table-container'>
    <table border="0">
        <col width="20%">
        <col width="80%">
        <tr>
        <td>8:30</td><td> Welcome to OpenCor Forum<br/>
                <em>Livy Real and Ivan Vladimir Meza Ruiz</em></td>
        </tr>
        <tr>
        <td>8h50 </td><td> OBras: a fully annotated and partially human-revised 
        corpus of Brazilian literary works in the public domain
<em>Diana Santos, Claudia Freitas and Eckhard Bick</em></td>
        </tr>
        <tr>
<td>9h20 </td><td> Portuguese Universal Dependencies via Bosque<br/>
<em>Valeria de Paiva, Claudia Freitas, Alexandre Rademaker, Livy Real and 
Fabricio Chalub</em></td>
        </tr>
        <tr>
        <td>9h50</td><td> The Fake.Br corpus - a corpus of fake news for 
        Brazilian Portuguese<br/>
<em>Roney L. De S. Santos, Rafael A. Monteiro and Thiago Pardo</em>
        </td>
        </tr>
        <tr>
        <td>10:10</td><td> A brief description of SICK-BR<br>
<em>Livy Real, Ana Rodrigues, Andressa Vieira E Silva, Bruna Thalenberg, Bruno 
Guide, Cindy Silva, Igor C. S. Câmara, Guilherme de Oliveira Lima, Rodrigo Souza 
and Valeria de Paiva</em></td>
        </tr>
<tr>
<td>10:30-11:00</td><td> COFFEE BREAK</td>
</tr>
<tr>
<td>11:00</td><td> CorPop: a corpus of popular Brazilian Portuguese</br>
<em>Bianca Pasqualini and Maria José B. Finatto</em></td>
</tr>

<tr>
<td>11:30</td><td> Invited talk: Mexican Corpora and their applications<br/>
<em>Alfonso Medina Urrea</em><br/>
</td>
</tr>
</table>

<p>

<strong>Invited Talk: <em>Mexican Corpora and their applications</em></strong>
<em>Speaker:</em> <a 
href="https://cell.colmex.mx/index.php/alfonso-medina">Alfonso Medina Urrea</a>, 
<a href="https://www.colmex.mx/">El Colegio de México</a>, <a 
href="http://www.redttl.mx">Red Temática de Tecnologías del Lenguaje</a>, 
Mexico<br>
<center> <strong>Abstract</strong></center><br>

Perhaps the oldest Latin American electronic corpus is the Corpus del español 
mexicano contemporáneo (CEMC), which was compiled in the seventies for 
lexicographical purposes. Since then, numerous corpora have appeared in Mexico 
for very diverse language technologies, like speech recognition, speech 
    synthesis, text mining applications, etc., and for the study of linguistic 
    phenomena in the synchronic and diachronic dimensions. In this talk, several 
    prominent text corpora projects will be described. We will briefly examine 
    their objectives, focused language, structure, tools and resources, context 
    of apparition, and their applications, among other features.
</p>

