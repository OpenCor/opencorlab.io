---
title: Accepted OpenCor 2018
date: 2018-09-17
---

* *OBras: a fully annotated and partially human-revised corpus of Brazilian literary works in the public domain.*<br/>
  _Diana Santos, Claudia Freitas and Eckhard Bick_.

* *The Fake.Br corpus – a corpus of fake news for Brazilian Portuguese.*<br/>
  _Roney L. De S. Santos, Rafael A. Monteiro and Thiago Pardo_.

* *Portuguese Universal Dependencies via Bosque.*</br>
  _Valeria de Paiva, Claudia Freitas, Alexandre Rademaker, Livy Real and Fabricio Chalub_.

* *On the Building of the Large Scale Corpus of Southern Qichwa.*</br>
  _Luis Camacho, Rodolfo Zevallos and Nelsi Melgarejo_.

* *Multimedia Corpora of Mexican Sign Language (MSL) with Syntactic Functions.*</br>
  _Obdulia Pichardo-Lagunas and Bella Martinez-Seis_.

* *CorPop: a corpus of popular Brazilian Portuguese.*</br>
  _Bianca Pasqualini and Maria José B. Finatto_.

* *HWxPI: A Multimodal Spanish Corpus for Personality Identification.*</br>
  _Gabriela Ramírez-De-La-Rosa, Esau Villatoro-Tello and Héctor Jiménez Salazar_.

* *A brief description of SICK-BR.*</br>
  _Livy Real, Ana Rodrigues, Andressa Vieira E Silva, Bruna Thalenberg, Bruno Guide, Cindy Silva, Igor C. S. Câmara, Guilherme de Oliveira Lima, Rodrigo Souza and Valeria de Paiva_.

* *The Wixarika-Spanish Parallel Corpus.*</br>
  _Jesús Manuel Mager Hois, Dionico Carrillo and Ivan Vladimir Meza Ruiz_.
