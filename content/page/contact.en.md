---
title: Contact
type: page
date: 2018-09-17
---

Any question may be sent to the organizers: livyreal [at] gmail.com; 
ivanvladimir [at] turing.iimas.unam.mx

If you want to follow the discussion and news about Open Corpora in the region 
join [the mail list](https://groups.google.com/forum/#!forum/opencor).


You can also follow us in [Twitter](https://twitter.com/OpenCorF).
