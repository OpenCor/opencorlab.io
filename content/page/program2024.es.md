---
title: Programa 2024
description: 
date: 2024-03-10
---

14 de marzo de 2024


| Hora | Título | Presenta |Autores |
|---|---|---|---|
| 14h30 | Opening | Livy Real |  |
| 14h40 | Training Large Language Encoders with the Curated Carolina Corpus | Paulo Cavalin | Guilherme Mello, Paulo Cavalin, Felipe Ribas Serras, Marcelo Finger, Pedro Domingues, Miguel de Mello Carpi and Marcos Jose |
| 15 | Carolina: a dual purpose corpus of contemporary Portuguese under
continuous development  | Marcelo Finger | Felipe Ribas Serras, Mariana Sturzeneker, Maria Clara Crespo, Mayara Feliciano Palma, Miguel de Mello Carpi, Aline Silva Costa, Guilherme Lamartine de Mello, Vanessa Monte, Cristiane Namiuti, Maria Clara Paixão De Sousa and Marcelo Finger |
| 15h20 | Overview of Latin American and Iberian corpora in Sketch Engine | František Kovařík | František Kovařík  |
| 15h40 | Factive Verbs in Portuguese | Livy Real | Valeria de Paiva and Livy Rea  |
| 16h   | Coffee break  |  |  |
| 16h30 | Rodaviva Corpus | Oto Vale | Gabriela Wick-Pedro, Isaac Souza de Miranda Jr and Oto Vale  |
| 16h50 | DOXCOR-br: Um corpus de discurso de ódio xenofóbico para o português brasileiro | Amanda Oliveira | Amanda Oliveira, Eduardo Luz and Maxilene Faria |
| 17h10 | RePro: A Benchmark Dataset for Opinion Mining in Brazilian Portuguese | Karina Soares | Lucas Nildaimon dos Santos Silva, Ana Claudia Bianchini Zandavalle, Carolina Francisco Gadelha Rodrigues, Tatiana da Silva Gama, Fernando Guedes Souza, Phillipe Derwich Silva Zaidan, Alice Florencio Severino da Silva, Karina Soares and Livy Real |


