---
title: OpenCor 2020
date: 2019-02-01
---

## Motivación

En años recientes el campo de Lingüística Computacional se ha consolidado gracias a la explotación de corpus más grandes, mejores y mejor anotados. La falta de cobertura en los corpus actuales es uno de los cuellos de botella para el procesamiento de lenguaje natural. Por otro lado, producir y mantener dichos recursos es una tarea compleja que muchas veces requiere tiempo, una cantidad considerable de fondos económicos y la cooperación de varios expertos. Esta situación, en la que por un lado necesitamos dichos corpus y por otro lado producirlos con una calidad adecuada y hacerla disponible no es un asunto trivial. Mientras que el “big data” es una tendencia cada vez más en el campo, producir corpus continúa siendo una tarea fuera de los focos en el Procesamiento del Lenguaje Natural, en particular para lenguajes diferentes del inglés. Así, estas lenguas o variantes poseen un menor número de datos a veces no necesariamente suficientes para metodologías de aprendizaje automático u otras. En esta situación es dificultoso publicar y debatir propiamente el trabajo que estos y estas investigadoras realizan. Además, muchos de los eventos más importantes de Procesamiento de Lenguaje Natural no son tan abiertos a descripciones de corpus. Por último, un caso especial es el de las lenguas minoritarias o en peligro de extinción donde tampoco existen demasiados foros donde debatir.

Las comunidades ibéricas y latinoamericanas que producen corpus abiertos tampoco tienen un evento ya establecido en el cual especialistas del área puedan compartir ideas y dificultades, además de obtener un feedback sobre su trabajo. Algunos encuentros especializados han sido celebrados en los últimos años, pero estos no han tenido continuidad o eran muy específicos y no abarcaban a toda la comunidad. Dadas estas condiciones, es común que grupos de países ibéricos y latinoamericanos que comparten intereses y desafíos, no se conozcan o no tengan conocimiento sobre trabajos recién producidos que pueden ser de gran interés.

El Foro OpenCor pretende crear un evento permanente para debatir sobre la producción, anotación y mantenimiento de corpus abiertos para lenguas latinoamericanas e ibéricas, y también crear una lista extensiva para estos recursos. OpenCor acepta trabajos sobre portugués y gallego, español, lenguas indígenas, catalán, aragonés, astur-leones, aranés y cualquier otra lengua hablada en Latinoamérica y en los países ibéricos. Los trabajos sobre lenguas minoritarias, en extinción y/o con pocos recursos son particularmente bienvenidos.

## El evento

Esta es la tercera edición del Foro OpenCor, una tentativa de agregar la comunidad que produce, mantiene y distribuye corpus abiertos para la gran variedad de lenguas habladas en Latinoamérica, en los países ibéricos o en inglés. Todos los trabajos aceptados formarán parte de la Lista OpenCor, una iniciativa de catalogar recursos abiertos producidos para esas lenguas.
El evento da la bienvenida, pero no está restringido, a los siguientes temas:

* Lanzamiento de nuevos corpus abiertos
* Descripción de corpus abiertos ya establecidos
* Directivas de creación, estrategias de anotación y mejores prácticas
* Mantenimiento y manejo de corpus
* Curación y verificación de corpus
* Diseño y evaluación de corpus
* Estrategias de creación de corpus y dificultades enfrentadas por la comunidad


## Envío de trabajos

El Foro OpenCor acepta resúmenes extendidos de hasta una páginas. Los resúmenes deben ser anónimos. Los envíos deben seguir el formato de LNCS sin abstract y pueden estar en una lengua de la Latinoaméricana o Iberérica o Inglés. Los resúmenes aceptados serviran como descripción de los recursos sometidos y aparecerán en la lista de descripción. OpenCor es no archivable, es decir, son bienvenidos los trabajos ya publicados o a ser publicados y el envío al OpenCor no impide futuras publicaciones.

Los autores y las autoras deben proporcionar en sus resúmenes el link de acceso a los recursos descritos. Se espera que esta lista ayude a la comunidad a divulgar y dar continuidad a estos corpus.

Como una de las mayores dificultades para las comunidades en cuestión es justamente la financiación, todos los trabajos aceptados estarán disponibles en la página del evento y aparecerán en la Lista OpenCor, independiente de la presencia de los autores y autoras en el día del encuentro. Con esa iniciativa se pretende que todos los grupos, incluso los que no tengan financiación, puedan tener sus recursos disponibles. Al enviar un trabajo, los autores tendrán que indicar si éste debe ser tenido en cuenta para la presentación o si prefieren proporcionar un video de cinco minutos en cualquier plataforma para reproducirlo durante el evento.

Liga para someter trabajos: https://easychair.org/conferences/?conf=opencor2019
[LaTex stylesheet](ftp://ftp.springernature.com/cs-proceeding/llncs/llncs2e.zip)
[MS Word stylesheet](ftp://ftp.springernature.com/cs-proceeding/llncs/word/splnproc1703.zip)

## Fechas

Envío de resúmenes: Febrero 10, 2020
Comunicación de aceptación: Febrero 17, 2020
Foro: Marzo 2 y 4, 2020


## Registro al foro

Esta edición es parte de [PROPOR 2020](https://propor.di.uevora.pt/). Habrá un costo de registro, para mayor informes revisar [la página oficial de PROPOR](https://propor.di.uevora.pt/registration/).


## Organización

* Livy Real – University of São Paulo / GLiC
* Ivan Vladimir Meza –  Universidad Nacional Autónoma de Mexico / IIMAS
* Adrian Pastor López Monroy –  CIMAT / Organizador Local

## Comité del programa

Pronto

## Contacto

Cualquier pregunta puede ser enviado a los organizadores: livyreal [at] gmail.com; ivanvladimir [at] turing.iimas.unam.mx
