---
title: Home
date: 2021-09-07
---

This year OpenCor Forum will be held as a part of [The 16th International Conference on Computational Processing of Portuguese (PROPOR 2024)](https://propor2024.citius.gal/):

* [Call for papers]({{< relref "cfp2024">}})
* [Program]({{< relref "program2024">}})

**What is it?**

Firstly, OpenCor is a **collaborative** list of open corpora for Latin American 
and Iberian Languages. OpenCor is also a discussion group aiming to have annual 
meetings to integrate these communities.

Everyone interested in the topic is welcome to join the 
[group](https://groups.google.com/forum/#!forum/opencor)

**Goals**

* Easy access/discovery of Latin American and Iberian Languages resources
* Integration
* Discussion
* Share ideas and resources
* Publish work on corpora
* Visibility
* Support

## Submit a corpus

Now it's possible to submit a corpus to be add to the list 

<a class="button"
href="https://docs.google.com/forms/d/e/1FAIpQLSddrbqqq2ee9edNIoYEYFHHSM0VRyrBZLNcSmQ29D5_EUYsFg/viewform?usp=sf_link"
target="_blank">Submit an Open corpus</a> 
