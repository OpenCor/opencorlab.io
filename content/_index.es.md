---
title: Página principal
date: 2019-07-23
---

Este año OpenCor Forum será parte de  [The 16th International Conference on Computational Processing of Portuguese (PROPOR 2024)](https://propor2024.citius.gal/):

* [Call for papers]({{< relref "cfp2024">}})
* [Programa]({{< relref "program2024">}})

**¿Qué es?**

Ante todo, Opencor es una lista **colaborativa** de los corpus abiertos para los lenguajes de América Latina e Ibéricos. OpenCor también es un un grupo de discusión cuyo objetivo es tener una reunión anual para integrr estas comunidades.

Todos aquellos interesados en estos temas deben unirse al [grupo](https://groups.google.com/forum/#!forum/opencor)

**Objetivos**

Para los recursos originarios de latinoamericanos y de los países ibéricos

* Fácil acceso y descubrimiento 
* Integración
* Discusión
* Compartir ideas y recursos
* Publicar 
* Visibilizar
* Apoyo


## Someter un corpus

Ahora es posible poner a considerción para su listado a un corpus

<a class="button"
href="https://docs.google.com/forms/d/e/1FAIpQLSddrbqqq2ee9edNIoYEYFHHSM0VRyrBZLNcSmQ29D5_EUYsFg/viewform?usp=sf_link"
target="_blank">Someter Open corpus</a> 
