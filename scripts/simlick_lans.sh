#!/bin/bash

for filename in `ls $1`; do
    if [[ $filename == *md ]]; then
        if [[ $filename != *".es."* ]]; then
            b="${filename%.md}"
            ln -s "$1/${filename}" "$1/${b}.es.md"
        fi
    fi
done
